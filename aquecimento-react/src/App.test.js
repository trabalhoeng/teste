import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Rotas from './Rotas';

import Listas from './pages/lista/Listas';
import Lista from './pages/lista/Lista';
import CriarLista from './pages/lista/CriarLista'; 

import { Route, MemoryRouter } from 'react-router-dom';

import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

let pathMap = {};

describe('Testando rotas da App lista-compra', () => {
  beforeAll(() => {
    const component = shallow(<Rotas />);
    pathMap = component.find(Route).reduce((pathMap, route) => {
      const routeProps = route.props();
      pathMap[routeProps.path] = routeProps.component;
      return pathMap;
    }, {});

  })
  it('/ carrega o componente inicio', () => {
    expect(pathMap['/']).toBe(Listas); 
  })

  it('/lista carrega o componente inicio', () => {
    expect(pathMap['/lista']).toBe(Lista); 
  })

  it('/CriarListas carrega o componente inicio', () => {
    expect(pathMap['/criarlista']).toBe(CriarLista); 
  }) 

  test('/Inicio utilizando Memory Route', () => {
    const envelope = mount(
      <MemoryRouter inicialEntries={['/']}><Rotas /></MemoryRouter>
    );
    expect(envelope.find(Listas)).toHaveLength(1);
  })

});